/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  XFadeSamplerVoice.cpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 03/12/16.
//
//

#include "XFadeSamplerVoice.hpp"
#include "XFadeSamplerSound.hpp"
#include "VelocityToGainMapper.hpp"
#include <sstream>



/*--------------------------------------------------------------------------------------------------------------
 * EnvParameters
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
void EnvParameters::addEnvParameter( float timeS, float gain)
{
	t.push_back(timeS);
	gains.push_back( gain );
}


void EnvParameters::setTime( int index, float timeS)
{
	if( index < (int) t.size()) {
		t.at(index) = timeS;
	}

}
void EnvParameters::setGain( int index, float gain)
{
	if( index < (int) t.size()) {
		gains.at(index) = gain;
	}
}

float EnvParameters::getTime( int index)
{
    if( index < (int) t.size()) {
        return t.at(index);
    }
    return 0.0f;
}


void EnvParameters::setParameter( int index, float timeS, float gain )
{
	if( index < (int)t.size() && index < (int)gains.size()) {
		t.at(index) = timeS;
		gains.at(index) = gain;
	} else {
		int nToAdd = index - t.size() -1;
		for( int i=0; i< nToAdd; i++) {
			addEnvParameter( 0.0f, 1.0f);
		}
		addEnvParameter(timeS, gain);
	}
}


void EnvParameters::setOAHDSR( float aOffsetS, float aAttackS, float aHoldS, float aDecayS, float aSustainGain, float aReleaseS )
{
	offsetS = aOffsetS;
	setGain0(0.0);
	setParameter(0,aAttackS,1.0);
	setParameter(1, aHoldS, 1.0);
	setParameter(2, aDecayS, aSustainGain);
	setReleaseS(aReleaseS);
}






/*--------------------------------------------------------------------------------------------------------------
 * XFadeSamplerVoice
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/
XFadeSamplerVoice::XFadeSamplerVoice(PluginParameters* aPluginParameters, SampleBin* aSampleBin,  ThreadPool *aDfdThreadPool)
:     SynthesiserVoice(), dfdThreadPool(aDfdThreadPool),pluginParameters( aPluginParameters), sampleBin( aSampleBin),
	  sound(NULL),
	  velocityGain(0.0f),soundLPanGain (0.0f), soundRPanGain (0.0f),
	  envReleaseLevel (0), envReleaseDelta (0),
	  envIsInRelease (false)
{
	clear();
}

XFadeSamplerVoice::~XFadeSamplerVoice()
{
}

bool XFadeSamplerVoice::canPlaySound (SynthesiserSound* aSound)
{
	return dynamic_cast<const XFadeSamplerSound*> (aSound) != nullptr;
}


bool XFadeSamplerVoice::isPlayingLayer( int layerNr )
{
	return( playingLayerNr == layerNr );

}

int XFadeSamplerVoice::getLayerNumber()
{
	return ( sound == NULL ? -1 : sound->soundDefinition->layer );
}

void XFadeSamplerVoice::clear()
{
	playingLayerNr = -1;
	soundGain = 1.0;
	nElements = 0;
	nrOfElementsPlaying = 0;
	velocity = 0.0;
	midiNote = -1;
    soundingMidiNote = -1;

	pressureCatched = false;
	firstElementToPlayNdx = 0;
	lastElementToPlayNdx = 0;
	
    oneHitOffsetS = -1.0f;
    oneHitReleaseS = -1.0f;
    oneHitAttackS = -1.0f;
	pitchWheelTuneSt = 0;
    keyGroupGain = 1.0f;
    keyGroupPan = 0.0f;
    keyGroupTuneSt = 0.0f;
    keyGroupTune = 0.0f;
	keyGroupBevel = 1.0f;
}


void XFadeSamplerVoice::setKeyGroup( KeyGroupDefinition* aKeyGroupDefinition )
{
	keyGroup = aKeyGroupDefinition;
}


void XFadeSamplerVoice::setKeyGroupParams( float aGain, float aPan, int aTuneSt, float aTuneCt, float aBevel ) 
{ 
	keyGroupGain = aGain;
	keyGroupPan = aPan;
	keyGroupTuneSt = aTuneSt;
	keyGroupTune = aTuneCt;
	keyGroupBevel = aBevel;
};

void XFadeSamplerVoice::startNote (const int aMidiNoteNumber,
		const float aVelocity,
		SynthesiserSound* aSound,
		const int /*currentPitchWheelPosition*/)
{

	sound = dynamic_cast<XFadeSamplerSound*> (aSound);

    dfd = *pluginParameters->dfdParam;
    dfdPreloadFrames = *pluginParameters->dfdPreloadFrames;
    dfdLoadAtFrames = dfdPreloadFrames - *pluginParameters->dfdLoadAtFramesLeft;
    dfdLoadFrames = *pluginParameters->dfdLoadFrames;
    dfdBufferTotalSize = dfdLoadFrames*2;
    dfdLoad0AtFrames =  dfdLoadFrames * 2 - *pluginParameters->dfdLoadAtFramesLeft;
    dfdLoad1AtFrames =  dfdLoadFrames - *pluginParameters->dfdLoadAtFramesLeft;

	playingLayerNr = sound->soundDefinition->layer;

	
	// soundTune shows the detune (sound params) in semitones
	float tune = (sound->soundDefinition->tuneCt / 100.0f) + *(pluginParameters->tuneStParam) + *(pluginParameters->tuneParam) + keyGroupTuneSt+ keyGroupTune;

	midiNote = aMidiNoteNumber;
	velocity = aVelocity;
	pressureCatched = false;



	elementPlayData.clear();
	nElements = sound->soundElements.size();
	//nrOfElementsPlaying = nElements;
    
    
    //see if this is a oneShot sample (drums) and override release time with -1, if so
    float envReleaseTime = *(pluginParameters->releaseTParam);
    if( sound->soundDefinition->oneShot) {
        envReleaseTime = -1;
    }

	envParameters.setOAHDSR(*(pluginParameters->offsetTParam),
			*(pluginParameters->attackTParam),
			*(pluginParameters->holdTParam),
			*(pluginParameters->decayTParam),
			*(pluginParameters->sustainGainParam),
			envReleaseTime);

	for( int i=0; i<nElements; i++) {
		SoundElementDefinition& sed = sound->soundDefinition->soundElementDefinitions[i];
		SampleBinSample* binSample = sound->soundElements[i].getRoundRobinBinSample();
		ElementPlayData epd;
		epd.pitchRatio = pow (2.0f, (soundingMidiNote - sound->soundDefinition->centerNote  + tune) / 12.0f) * binSample->sampleRate / (float)getSampleRate();

		int offsetSamples = (int) (envParameters.offsetS * binSample->sampleRate );
		if( oneHitOffsetS >= 0.0f ) {
			offsetSamples = (int) (oneHitOffsetS * binSample->sampleRate );
		}

		epd.sourceSamplePosition = (float)offsetSamples;

		epd.nrOfSamples = binSample->numberOfFrames;
		epd.inL = (float*) binSample->framesBuffer->getReadPointer (0);
		epd.inR = (float*) (binSample->framesBuffer->getNumChannels() > 1 ? binSample->framesBuffer->getReadPointer (1) : nullptr);
		epd.gain = Decibels::decibelsToGain(sed.gainDb);
		epd.velocityRangeFrom = sed.velocityRangeFrom;
		epd.velocityRangeTo = sed.velocityRangeTo;
		epd.xFadeVelocityTo = sed.xFadeVelocityTo;
		epd.ringBuffer = NULL;
        epd.binSample = binSample;
        epd.ringBuffer = NULL;
        //epd.xFadeGain = 1.0;
        epd.elementGain = 1.0f;

		if( dfd && binSample->appliesForDFD) {
            if(offsetSamples > dfdLoadAtFrames) {
                // offset can't be greater that 0.75*preloadBufferSize
                // we could also stop the element in this case.
                offsetSamples = dfdLoadAtFrames;
            }
            epd.ringBuffer = sampleBin->requestRingBuffer();
            if( epd.ringBuffer ) {
                epd.playing = true;
                elementPlayData.push_back(epd);
            } else {
                // THROW AN ERROR    OUT OF RINGBUFFERS
            }
        } else {
            epd.playing = true;
            elementPlayData.push_back(epd);
        }
        nrOfElementsPlaying = elementPlayData.size();
	}


	soundLPanGain =  (sound->soundDefinition->pan <= 0 ? 1.0f : (1.0f-sound->soundDefinition->pan));
	soundRPanGain =  (sound->soundDefinition->pan >= 0 ? 1.0f : (1.0f+sound->soundDefinition->pan));

	envIsInRelease = false;
	envReleaseLevel = 1.0f;
	envCurrentPointNdx = -1;
	envGain = 1.0;

	if( oneHitAttackS >= 0.0f && oneHitAttackS >= envParameters.getTime(0)) {
        //set oneHitAttack only if its larger than the envelopes natural attack.
        //otherwise shorter legato/pitchRetrigger attack would override longer envelope attack !
		envParameters.setParameter(0, oneHitAttackS, 1.0f);
		//reset oneHitAttackS because this is for one hit only
		oneHitAttackS = 0.0f;
	}
	//reset oneHitOffsetS and oneHitReleaseS because this is for one hit only
	oneHitOffsetS = -1.0f;
	oneHitReleaseS = -1.0f;

	envNrOfPoints = envParameters.t.size();
	//envDbCurrent = envParameters->db0;
	envGainNext = envParameters.gain0;
	envGainDelta = 0.0;
	envGain = envParameters.gain0;
	envSamples = -1;
	envCurrentPointNdx++;
	if( envCurrentPointNdx < envNrOfPoints) {
        envGainNext = envParameters.gains.at(envCurrentPointNdx);
		envSamples = envParameters.t.at(envCurrentPointNdx) * (float)getSampleRate() +1.0f;
		envGainDelta = (envGainNext-envGain) / envSamples;
	}

	soundGain = Decibels::decibelsToGain( sound->soundDefinition->gainDb);

	

}

void XFadeSamplerVoice::stopNote (float /*velocity*/, bool allowTailOff)
{
	if (allowTailOff) {
		if( oneHitReleaseS >= 0.0f) {
			//if there is a onehit release time set (e.g. for retrigger), then
			//recalculate the release parameters for this new release time
			envReleaseSamples = (int)(oneHitReleaseS * (float)getSampleRate());
			if (envReleaseSamples > 0)
				envReleaseDelta = (float) (-1.0 / envReleaseSamples);
			else
				envReleaseDelta = -1.0f;
		} else {
            if( sound->soundDefinition->oneShot) {
                //OneShot (Drums) mode.
                envReleaseDelta = 0.0;
            } else {
                envReleaseSamples = (int)(envParameters.releaseS * (float)getSampleRate());
                if (envReleaseSamples > 0)
                    envReleaseDelta = (float) (-1.0 / envReleaseSamples);
                else
                    envReleaseDelta = -1.0f;
            }
        }
		envIsInRelease = true;
	} else {
        sound = NULL;
		playingLayerNr = -1;
		clearCurrentNote();
        for( int i=0; i<nElements; i++) {
            ElementPlayData& epd = elementPlayData.at(i);

            if( epd.ringBuffer != NULL) {
                sampleBin->returnRingBuffer(epd.ringBuffer);
            }
        }
	}
	//reset oneHitReleaseS because this is for one hit only
	oneHitReleaseS = -1.0f;
}

void XFadeSamplerVoice::pitchWheelMoved (const int newValue)
{
	//float soundTune = (sound->soundDefinition->tuneCt / 100.0f);
	//float pluginTune = pluginParameters->tuneParam->get();

	float tune = (sound->soundDefinition->tuneCt / 100.0f) + *(pluginParameters->tuneStParam) + *(pluginParameters->tuneParam) + keyGroupTuneSt+ keyGroupTune;


    

	float pbVal = (float)newValue - 8192.0f;
	float st = 0;
	if( pbVal < 0 ) {
		st = (pbVal / 8192.0f * *(pluginParameters->pitchWheelRangeParam));
	} else {
		st = (pbVal / 8191.0f * *(pluginParameters->pitchWheelRangeParam));
	}

	for( int i=0; i<nElements; i++) {
		ElementPlayData& epd = elementPlayData.at(i);
        SampleBinSample* binSample = sound->soundElements[i].getRoundRobinBinSample();
        
		//epd.pitchRatio = pow (2.0f, (soundingMidiNote - sound->soundDefinition->centerNote + st + soundTune + pluginTune) / 12.0f);
        epd.pitchRatio = pow (2.0f, (soundingMidiNote - sound->soundDefinition->centerNote  + st + tune) / 12.0f) * binSample->sampleRate / (float)getSampleRate();
        
    }

}

void XFadeSamplerVoice::controllerMoved (const int /*controllerNumber*/, const int /* newValue*/)
{
}

void XFadeSamplerVoice::pressureMoved( float newValue)
{
	calcXFade(newValue);
}


void XFadeSamplerVoice::pressureCatchUp( float newValue)
{
	if( !pressureCatched && newValue >= velocity) {
		pressureCatched = true;
	}
	if( pressureCatched) {
		pressureMoved( newValue );
	}
}

void XFadeSamplerVoice::pressureCatchDown( float newValue)
{
	if( !pressureCatched && newValue <= velocity) {
		pressureCatched = true;
	}
	if( pressureCatched) {
		pressureMoved( newValue );
	}

}




void XFadeSamplerVoice::calcXFade( float aPressure )
{
	firstElementToPlayNdx = -1;
	lastElementToPlayNdx = -1;


	float veloSens = *(pluginParameters->veloSensParam);
	velocityGain = VelocityToGainMapper::velocityToGain(veloSens,  aPressure);
    //std::stringstream log1;
    //log1 << "VelGain Pressure=" << aPressure << "  gain=" << velocityGain << "\n";
    //Logger::getCurrentLogger()->writeToLog(log1.str());
    

	for( int i=0; i<nElements; i++) {
		ElementPlayData& epd = elementPlayData[i];
		float xFadeGain = 0.0;
		if( sound->soundDefinition->xFade) {
			if( aPressure > epd.velocityRangeFrom && aPressure <= epd.velocityRangeTo ) {
				//fadeIn
				float veloInRange = (aPressure - epd.velocityRangeFrom) / (epd.velocityRangeTo-epd.velocityRangeFrom);
				xFadeGain = veloInRange;
			} else if( aPressure > epd.velocityRangeTo && aPressure <= epd.xFadeVelocityTo ) {
				//fadeOut
				float veloInRange = 1.0f-((aPressure - epd.velocityRangeTo) / (epd.xFadeVelocityTo-epd.velocityRangeTo));
				xFadeGain = veloInRange;
			} else {
				xFadeGain = 0.0;
			}
		} else {
			if( velocity > epd.velocityRangeFrom && velocity <= epd.velocityRangeTo) {
				xFadeGain = 1.0f;
			} else {
                xFadeGain = 0.0f;
			}
		}
		epd.elementGain = xFadeGain * velocityGain * epd.gain;
        //std::stringstream log2;
        //log2 << "VelGain epd.gain=" << epd.gain << "  epd.elementGain=" << epd.elementGain;
        //Logger::getCurrentLogger()->writeToLog(log2.str());
		if( epd.elementGain > 0.0f) {
			//epd.sounding = true;
			if( firstElementToPlayNdx == -1 || i < firstElementToPlayNdx)
				firstElementToPlayNdx = i;
			if( i > lastElementToPlayNdx )
				lastElementToPlayNdx = i;
		}
	}
	if( *pluginParameters->pressureModeParam != PatchDefinition::PRESSUREMODE::VELOCITY ) {
		// we can reduce the playing elements only if in velocity mode, because there the
		// pressure stays stable. In all other cases we have to process all elements, even
		// if we dont actually hear them.
		firstElementToPlayNdx = 0;
		lastElementToPlayNdx = nElements-1;
	}
    
}


//==============================================================================
void XFadeSamplerVoice::renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples)
{
	if (sound != NULL)
	{

        if( firstElementToPlayNdx == -1 )
            return;
    
        pluginGain = pluginParameters->gainParam->get() * pluginParameters->layerGainParams[ playingLayerNr ]->get() * keyGroupGain * keyGroupBevel;
        pluginPan = pluginParameters->panParam->get() + keyGroupPan;
		//keep pluginPan in range -1.0 .. 1.0
		pluginPan = (pluginPan < -1.0f ? -1.0f : (pluginPan > 1.0f? 1.0f : pluginPan));
        pluginLGain = (pluginPan <= 0 ? 1.0f : (1.0f-pluginPan) );
        pluginRGain = (pluginPan >= 0 ? 1.0f : (1.0f+(pluginPan)));
    
        summedLGain = soundLPanGain*soundGain * pluginGain * pluginLGain;
        summedRGain = soundRPanGain*soundGain * pluginGain * pluginRGain;
    
        outL = outputBuffer.getWritePointer (0, startSample);
        outR = outputBuffer.getNumChannels() > 1 ? outputBuffer.getWritePointer (1, startSample) : nullptr;



        // IF in DFD mode, then check for each playing soundelement, if dfd buffers have to get updated
        // and update them im background thream, if necessary.
        // Ensure that dfdLoadAtFrames, dfdLoad0AtFrames, dfdLoad1AtFrames have proper values
        // (  preBufferSize-dfdLoadAtFrames > numSamples !!)
        // (  bufferHalfSize-dfdLoad0AtFrames > numSamples !!)
        // (  bufferSize-dfdLoad1AtFrames > numSamples !!)
        if( dfd ) {
			for( int i=firstElementToPlayNdx; i<=lastElementToPlayNdx; i++) {
				ElementPlayData& epd = elementPlayData[i];
                tmpRingBuf = epd.ringBuffer;
                if( tmpRingBuf != nullptr) {
					samplePos = (int) epd.sourceSamplePosition;
					if( samplePos < dfdPreloadFrames ) {
						if( samplePos > dfdLoadAtFrames && !tmpRingBuf->buf0Requested) {
							framesLeft = epd.nrOfSamples - samplePos;
							framesToLoad = jmin(dfdLoadFrames,framesLeft);
							SampleLoader* aSampleLoader = new SampleLoader( dfdThreadPool, epd.binSample, tmpRingBuf, 0, dfdPreloadFrames, framesToLoad);
							dfdThreadPool->addJob(aSampleLoader, true);
							tmpRingBuf->buf0Requested=true;
							tmpRingBuf->buf1Requested=false;
						}
					} else {
						ndxInBuffer = (samplePos - dfdPreloadFrames) % (dfdLoadFrames*2);
						if( ndxInBuffer > dfdLoad0AtFrames && !tmpRingBuf->buf0Requested ) {
							framesLeft = epd.nrOfSamples - samplePos;
							framesToLoad = jmin(dfdLoadFrames,framesLeft);
							fromFrame = (((samplePos-dfdPreloadFrames) / dfdLoadFrames) +1) * dfdLoadFrames + dfdPreloadFrames;
							SampleLoader* aSampleLoader = new SampleLoader( dfdThreadPool, epd.binSample, tmpRingBuf, 0, fromFrame, framesToLoad);
							dfdThreadPool->addJob(aSampleLoader, true);
							tmpRingBuf->buf0Requested=true;
							tmpRingBuf->buf1Requested=false;
						} else if( ndxInBuffer > dfdLoad1AtFrames && ndxInBuffer < dfdLoadFrames && !tmpRingBuf->buf1Requested ) {
							framesLeft = epd.nrOfSamples - samplePos;
							framesToLoad = jmin(dfdLoadFrames,framesLeft);
							fromFrame = (((samplePos-dfdPreloadFrames) / dfdLoadFrames) +1) * dfdLoadFrames + dfdPreloadFrames;
							SampleLoader* aSampleLoader = new SampleLoader( dfdThreadPool, epd.binSample, tmpRingBuf, dfdLoadFrames, fromFrame, framesToLoad);
							dfdThreadPool->addJob(aSampleLoader, true);
							tmpRingBuf->buf0Requested=false;
							tmpRingBuf->buf1Requested=true;
						}
					}
				}
			}
        }

		while (--numSamples >= 0){
			l = 0;
			r = 0;
            
			//  WE HAVE TO TRANSPORT ALL OF THE ELEMENTS FORWARD, SO  A  POS (SourceSamplePosition)  FOR THE VOICE INSTEAD OF ELEMENT WOULD BE RECOMMENDABLE !
			//   BUT ELEMENT INDEPENDENT LOOPING WOULD NEED ELEMENT INDEPENDENT POS !

			for( int i=firstElementToPlayNdx; i<=lastElementToPlayNdx; i++) {
				ElementPlayData& epd = elementPlayData[i];
                tmpRingBuf = epd.ringBuffer;
				if( epd.playing) {
					pos = (int) epd.sourceSamplePosition;
					posPlusOne = pos+1;
					alpha = (float) (epd.sourceSamplePosition - pos);
					invAlpha = 1.0f - alpha;

					// it is not necessary to calc signal for elements that dont participate
					// in this specific velo range !
					if( posPlusOne < epd.nrOfSamples) {
						if( dfd && epd.ringBuffer != NULL) {
							if( pos >= dfdPreloadFrames) {
								pos = (pos-dfdPreloadFrames) % (dfdBufferTotalSize);
								el0 = (tmpRingBuf->inL [pos]);
								er0 = (tmpRingBuf->inR != nullptr ? tmpRingBuf->inR[pos] : el0);
							} else {
								el0 = (epd.inL [pos]);
								er0 = (epd.inR != nullptr ? epd.inR[pos] : el0);
							}
							if( posPlusOne >= dfdPreloadFrames) {
								posPlusOne = (posPlusOne-dfdPreloadFrames) % (dfdBufferTotalSize);
								el1 = (tmpRingBuf->inL [posPlusOne]);
								er1 = (tmpRingBuf->inR != nullptr ? tmpRingBuf->inR[posPlusOne] : el1);
							} else {
								el1 = (epd.inL [posPlusOne]);
								er1 = (epd.inR != nullptr ? epd.inR[pos] : el1);
							}
                        } else {
                            el0 = (epd.inL [pos]);
                            er0 = (epd.inR != nullptr ? epd.inR[pos] : el0);
                            el1 = (epd.inL [posPlusOne]);
                            er1 = (epd.inR != nullptr ? epd.inR[pos] : el1);
                        }

						l += (el0 * invAlpha + el1 * alpha) * epd.elementGain;
						r += (er0 * invAlpha + er1 * alpha) * epd.elementGain;
					}

					epd.sourceSamplePosition += epd.pitchRatio;

					if (epd.sourceSamplePosition >= epd.nrOfSamples){
						epd.playing = false;
						nrOfElementsPlaying--;
						if( nrOfElementsPlaying <= 0) {
							stopNote (0.0f, false);
							break;
						}
					}
				}
			}

            
			if (envIsInRelease)
			{
				l *= envGain*envReleaseLevel;
				r *= envGain*envReleaseLevel;

				envReleaseLevel += envReleaseDelta;
				if( !std::isfinite(envReleaseLevel)) {
					envReleaseLevel = 0.0f;
				}
				if (envReleaseLevel <= 0.0f) {
					stopNote (0.0f, false);
					break;
				}
			} else {
				if( envSamples > 0) {
					envGain = envGain + envGainDelta;
					envSamples--;
				} else if( envSamples == -1) {
					// let everything as is
					if (envGain <= 6.3e-8)
					{
						stopNote (0.0f, false);
						break;
					}
				} else {
					// go to next env point
					//envDbCurrent = envDbNext;
					envGainDelta = 0.0;
					envSamples = -1;
					envCurrentPointNdx++;
					if( envCurrentPointNdx < envNrOfPoints) {
						envGain = envGainNext;
						envSamples = envParameters.t.at(envCurrentPointNdx) * (float)getSampleRate() +1.0f;
						envGainNext = envParameters.gains.at(envCurrentPointNdx);
						envGainDelta = (envGainNext-envGain) / envSamples ;
					}
				}
				if( !std::isfinite(envGain)) {
					envGain = 0.0f;
				}
				l *= envGain;
				r *= envGain;

			}
             
			l *=  summedLGain;
			r *=  summedRGain;

            

			if (outR != nullptr){
				*outL++ += l;
				*outR++ += r;
			} else {
				*outL++ += (l + r) * 0.5f;
			}
		}
    
	}
}





