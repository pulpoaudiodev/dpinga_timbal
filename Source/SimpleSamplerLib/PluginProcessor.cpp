/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/


#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "XFadeSamplerSound.hpp"
#include "XFadeSamplerVoice.hpp"
#include "SamplerErrors.hpp"
#include <sstream>
#include <time.h>       /* time */
#include "Configurator.hpp"
#include "../ProductDef.h"


/*--------------------------------------------------------------------------------------------------------------
 * SimpleSamplerComponentAudioProcessor
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/


SimpleSamplerComponentAudioProcessor::SimpleSamplerComponentAudioProcessor()
:   
#ifndef JucePlugin_PreferredChannelConfigurations
	AudioProcessor(BusesProperties()
#if ! JucePlugin_IsMidiEffect
#if ! JucePlugin_IsSynth
		.withInput("Input", AudioChannelSet::stereo(), true)
#endif
		.withOutput("Output", AudioChannelSet::stereo(), true)
#endif
	),
#endif
	sampler(&pluginParameters), dfdThreadPool(new ThreadPool(1))
{
    editor = nullptr;

  	srand ((unsigned int)time(NULL));
    
    #ifdef DOLOGGING
    flogger = FileLogger::createDateStampedLogger (JucePlugin_Name,JucePlugin_Name+"_", ".txt", "Hi");
    Logger::setCurrentLogger(flogger);
    Logger::getCurrentLogger()->writeToLog("AudioProcessor started\n");
    #endif

    sourceSampleRate = 44100;    // adapt this for other sample sets !
    currentPatchNdx = -1;
    currentPatchDefinition = NULL;
    
    //addListener( &paramListener );
    
    File f=File::getSpecialLocation(File::currentApplicationFile);
    File d=f.getParentDirectory();
    aPathToAU = d.getFullPathName();

    createParameters();

    loadSampleBin();
    
    setCurrentProgram(0);

}




SimpleSamplerComponentAudioProcessor::~SimpleSamplerComponentAudioProcessor()
{
    cleanupPatches();
    uiConfig.cleanup();
    Logger::setCurrentLogger (nullptr);
}



/*===========================================
 CONFIGURATION
 ===========================================*/


void SimpleSamplerComponentAudioProcessor::cleanupPatches()
{
    // TODO:
    // make patchBank a non-pointer vector and make only clear() here.

    /*for( int i=0; i<(int)patchBank.size();i++) {
        if( patchBank.at(i) != NULL) {
            delete patchBank.at(i);
        }
    }
    patchBank.clear();*/
    
    patchBank.clear();
    
}





bool SimpleSamplerComponentAudioProcessor::loadSampleBin() {
	// Load the entire sample bin into data model.
    try {
        juce::String sampleBinName = JucePlugin_Name;  sampleBinName += ".bin";
        juce::String sampleBinPath = aPathToAU + "/" + sampleBinName;
        //sampleBin= new SampleBin();
        if( sampleBin.open(sampleBinPath.toStdString())) {
            sampleBin.read();
            if( sampleBin.isReady()) {
                loadPatches();
				loadUIConfiguration();
            } else {
                throw LoadingError("SampleBin", "Bin not ready.");
            }
        } else {
            throw LoadingError(sampleBinPath.toStdString(), "Could not open bin file.\n Place "+sampleBinName+" into " + aPathToAU);
        }
        return true;
    } catch(LoadingError error) {
        AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, error.fileName, error.errorDescription, "OK", NULL,NULL);
        jassertfalse;
        return false;
    }
}

bool SimpleSamplerComponentAudioProcessor::loadPatches()
{
	// Load a setup XML file, if it exists in the current directory
	// This setup XML string overrides the one that is possible loaded with the sample bin
    juce::String configurationXMLFileName = JucePlugin_Name; configurationXMLFileName += ".xml";
    juce::String configurationXMLFilePath = aPathToAU + "/" + configurationXMLFileName;
    juce::File file2( configurationXMLFilePath );
    if( file2.existsAsFile()) {
        if( !sampleBin.loadSetupXMLFromFile( configurationXMLFilePath.toStdString() )) {
            throw LoadingError("Config file", "Could not load configuration file.");
        }
    }
	
	// Load the configuration (setup XML) into data model
    if( sampleBin.getSetupXML().size() > 0) {
		Configurator configurator;
		if( !configurator.configurePatchBankFromXML(sampleBin.getSetupXML(), patchBank )) {
			cleanupPatches();
            throw LoadingError("SampleBin", "Could not load patch configuration.");
		}
    } else {
        throw LoadingError("SampleBin", "No configuration available in bin.");
    }
    return true;
}



bool SimpleSamplerComponentAudioProcessor::loadUIConfiguration()
{
	// Load a background image, if it exists in the current directory
	// This background image overrides the one that is possible loaded with the sample bin
    juce::String backgroundFileName = JucePlugin_Name; backgroundFileName += ".png";
    juce::String backgroundFilePath = aPathToAU + "/" + backgroundFileName;
    juce::File file( backgroundFilePath );
    if( file.existsAsFile()) {
        if( !sampleBin.loadBackgroundFromFile( backgroundFilePath.toStdString() )) {
            throw LoadingError("SampleBin", "Could not load background file.");
        }
    }

    // Load a setup XML file, if it exists in the current directory
    // This setup XML string overrides the one that is possible loaded with the sample bin
    juce::String configurationXMLFileName = JucePlugin_Name; configurationXMLFileName += ".xml";
    juce::String configurationXMLFilePath = aPathToAU + "/" + configurationXMLFileName;
    juce::File file2( configurationXMLFilePath );
    if( file2.existsAsFile()) {
        if( !sampleBin.loadSetupXMLFromFile( configurationXMLFilePath.toStdString() )) {
            throw LoadingError("Config file", "Could not load configuration file.");
        }
    }
    
    
    // Load the configuration (setup XML) into data model
    if( sampleBin.getSetupXML().size() > 0) {
		Configurator configurator;
		if( !configurator.configureUIElementsFromXML( sampleBin.getSetupXML(), &sampleBin, uiConfig)) {
            uiConfig.cleanup();
            throw LoadingError("SampleBin", "Could not load ui configuration.");
		}
    } else {
        throw LoadingError("SampleBin", "No configuration available in bin.");
    }
    return true;
}


void SimpleSamplerComponentAudioProcessor::reloadPatches()
{
    try{
        if( !sampleBin.isReady()) {
            throw LoadingError("SampleBin", "SampleBin is not ready.");
        }
        std::string aPatchName;
        if( currentPatchDefinition != NULL) {
            aPatchName = currentPatchDefinition->getName();
        }
        currentPatchDefinition = NULL;
        currentPatchNdx = -1;
        cleanupPatches();
        loadPatches();
        //int ndx = findPatchIndex(aPatchName);
        int ndx = patchBank.getPatchDefinitionNdx(aPatchName);
        if( ndx > -1) {
            setCurrentProgram(ndx);
        }

        
    } catch(LoadingError error) {
        AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, error.fileName, error.errorDescription, "OK", NULL,NULL);
    }
}


void SimpleSamplerComponentAudioProcessor::reloadUI()
{
    try{
        if( !sampleBin.isReady()) {
            throw LoadingError("SampleBin", "SampleBin is not ready.");
        }
        if( editor != NULL) {
            editor->removeUIElements();
            uiConfig.cleanup();
            loadUIConfiguration();
            editor->setBackgroundImg(&sampleBin.getBackgroundImage());
            /*editor->setRotarySliderImg(&sampleBin.getRotarySliderImage(), uiConfigurations.rotarySliderWidth, uiConfigurations.rotarySliderHeight);
            editor->setToggleButtonImage(&sampleBin.getToggleButtonImage(), uiConfigurations.toggleButtonWidth, uiConfigurations.toggleButtonHeight);*/
            editor->recreateUIElements(&uiConfig);
        }
    } catch(LoadingError error) {
        AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, error.fileName, error.errorDescription, "OK", NULL,NULL);
    }
}






/*===========================================
    CREATE PARAMETERS
 ===========================================*/

void SimpleSamplerComponentAudioProcessor::createParameters()
{
    
    // This creates our parameters. We'll keep some raw pointers to them in this class,
    // so that we can easily access them later, but the base class will take care of
    // deleting them for us.
    addParameter (pluginParameters.gainParam  = new AudioParameterFloat ("gain",  "Gain", Decibels::decibelsToGain( -140.0f), Decibels::decibelsToGain(24.0f), Decibels::decibelsToGain(0.0f)));
    addParameter( pluginParameters.panParam = new AudioParameterFloat( "pan", "Pan", -1.0f, 1.0f, 0.0f));
    addParameter( pluginParameters.tuneStParam = new AudioParameterInt( "tuneSt", "TuneSt", -24, 24, 0));
    addParameter( pluginParameters.tuneParam = new AudioParameterFloat( "tune", "Tune", -0.5f, 0.5f, 0.0f));
    addParameter( pluginParameters.veloSensParam = new AudioParameterFloat( "veloSens", "VeloSens", 0.0, 2.0, 1.0));
    addParameter( pluginParameters.bevelParam = new AudioParameterFloat( "bevel", "Bevel", -1.0, 1.0, 0.0));
    
    addParameter (pluginParameters.offsetTParam  = new AudioParameterFloat ("offsetT",  "Offset",      0.0f, 20.0f, 0.0f));
    addParameter (pluginParameters.attackTParam  = new AudioParameterFloat ("attackT",  "Attack",      0.0f, 20.0f, 0.0f));
    addParameter (pluginParameters.holdTParam  = new AudioParameterFloat ("holdT",  "Hold",      0.0f, 20.0f, 0.0f));
    addParameter (pluginParameters.decayTParam  = new AudioParameterFloat ("decayT",  "Decay",      0.0f, 20.0f, 0.0f));
    addParameter (pluginParameters.sustainGainParam  = new AudioParameterFloat ("sustainDb",  "Sustain",      Decibels::decibelsToGain( -140.0f), Decibels::decibelsToGain(0.0f), Decibels::decibelsToGain(0.0f)));
    addParameter (pluginParameters.releaseTParam  = new AudioParameterFloat ("releaseT",  "Release",      0.0f, 20.0f, 0.1f));
    
    for( int i=0; i<MAX_NR_OF_LAYERS; i++) {
        std::stringstream paramLayerGainName; paramLayerGainName << "layer" << i << "GainDb";
        addParameter( pluginParameters.layerGainParams[i] = new AudioParameterFloat (paramLayerGainName.str(),  paramLayerGainName.str(), Decibels::decibelsToGain( -60.0f), Decibels::decibelsToGain(12.0f), Decibels::decibelsToGain(0.0f)));
        
    }
    /*
    addParameter( pluginParameters.layerGainParams[0] = new AudioParameterFloat ("layer1Gain",  "Layer1Gain", Decibels::decibelsToGain( -60.0f), Decibels::decibelsToGain(12.0f), Decibels::decibelsToGain(0.0f)));
    addParameter( pluginParameters.layerGainParams[1] = new AudioParameterFloat ("layer2Gain",  "Layer2Gain", Decibels::decibelsToGain( -60.0f), Decibels::decibelsToGain(12.0f), Decibels::decibelsToGain(0.0f)));
    addParameter( pluginParameters.layerGainParams[2] = new AudioParameterFloat ("layer3Gain",  "Layer3Gain", Decibels::decibelsToGain( -60.0f), Decibels::decibelsToGain(12.0f), Decibels::decibelsToGain(0.0f)));
    addParameter( pluginParameters.layerGainParams[3] = new AudioParameterFloat ("layer4Gain",  "Layer4Gain", Decibels::decibelsToGain( -60.0f), Decibels::decibelsToGain(12.0f), Decibels::decibelsToGain(0.0f)));
    */
    
	for( int i=0; i<MAX_NR_OF_KEYGROUPS; i++) {
		std::stringstream paramMonoName; paramMonoName << "kg" << i << "Mono";
        AudioParameterBool* paramKgMono  = new AudioParameterBool (paramMonoName.str(),paramMonoName.str(),false);
        pluginParameters.kgMonoParamsVec.push_back(paramKgMono);
		addParameter( paramKgMono );
        
		std::stringstream paramGainName; paramGainName << "kg" << i << "GainDb";
        AudioParameterFloat* paramKgGain = new AudioParameterFloat (paramGainName.str(),paramGainName.str(),Decibels::decibelsToGain( -140.0f), Decibels::decibelsToGain(24.0f), Decibels::decibelsToGain(0.0f));
        pluginParameters.kgGainParamsVec.push_back(paramKgGain);
		addParameter( paramKgGain );
        
		std::stringstream paramPanName; paramPanName << "kg" << i << "Pan";
        AudioParameterFloat* paramKgPan = new AudioParameterFloat (paramPanName.str(),paramPanName.str(),-1.0f,1.0f,0.0f);
        pluginParameters.kgPanParamsVec.push_back(paramKgPan);
		addParameter( paramKgPan);
        
		std::stringstream paramTuneStName; paramTuneStName << "kg" << i << "TuneSt";
        AudioParameterInt* paramKgTuneSt = new AudioParameterInt( paramTuneStName.str(), paramTuneStName.str(), -24, 24, 0);
        pluginParameters.kgTuneStParamsVec.push_back(paramKgTuneSt);
		addParameter( paramKgTuneSt );
        
		std::stringstream paramTuneName; paramTuneName << "kg" << i << "Tune";
        AudioParameterFloat* paramKgTune = new AudioParameterFloat( paramTuneName.str(), paramTuneName.str(), -0.5f, 0.5f, 0.0f);
        pluginParameters.kgTuneParamsVec.push_back(paramKgTune);
		addParameter( paramKgTune);
        
		std::stringstream paramBavelName; paramBavelName << "kg" << i << "Bevel";
        AudioParameterFloat* paramKgBevel = new AudioParameterFloat(paramBavelName.str(), paramBavelName.str(), -1.0, 1.0, 0.0);
        pluginParameters.kgBevelParamsVec.push_back(paramKgBevel);
		addParameter( paramKgBevel);
	}
	
	
    StringArray pressureModeChoices;
    pressureModeChoices.add( "Velocity");
    pressureModeChoices.add( "Pressure Abs");
    pressureModeChoices.add( "Pressure Rel");
    addParameter( pluginParameters.pressureModeParam = new AudioParameterChoice("pressureMode","PressureMode",pressureModeChoices,0));


    StringArray pressureControllerChoices = getControllersStringArray();
    addParameter( pluginParameters.pressureControllerParam = new AudioParameterChoice("pressureController","pressureController",pressureControllerChoices,1));
    
    
    StringArray pitchWheelModeChoices;
    pitchWheelModeChoices.add( "Continuous");
    pitchWheelModeChoices.add( "Semitones");
    pitchWheelModeChoices.add( "St Retrigger");
    addParameter( pluginParameters.pitchWheelModeParam = new AudioParameterChoice("pitchWheelMode","PitchWheelMode",pitchWheelModeChoices,0));
    addParameter( pluginParameters.pitchWheelRangeParam = new AudioParameterInt("pitchWheelRange","PitchWheelRange",0,24,12));
    addParameter( pluginParameters.pitchWheelRetriggerReleaseTParam = new AudioParameterFloat("pitchWheelRetriggerReleaseT","PitchWheelRetriggerReleaseT",0.0f,20.0f,0.005f));
    addParameter( pluginParameters.pitchWheelRetriggerOffsetTParam = new AudioParameterFloat("pitchWheelRetriggerOffsetT","PitchWheelRetriggerOffsetT",0.0f,20.0f,0.000f));
    addParameter( pluginParameters.pitchWheelRetriggerAttackTParam = new AudioParameterFloat("pitchWheelRetriggerAttackT","PitchWheelRetriggerAttackT",0.0f,20.0f,0.005f));
    
    
    
    StringArray legatoModeChoices;
    legatoModeChoices.add( "Off");
    legatoModeChoices.add( "Simple");
    addParameter( pluginParameters.legatoModeParam = new AudioParameterChoice("legatoMode","LegatoMode",legatoModeChoices,0));
    addParameter( pluginParameters.legatoReleaseTParam = new AudioParameterFloat("legatoReleaseT","LegatoReleaseT",0.0f,20.0f,0.005f));
    addParameter( pluginParameters.legatoOffsetTParam = new AudioParameterFloat("legatoOffsetT","LegatoOffsetT",0.0f,20.0f,0.000f));
    addParameter( pluginParameters.legatoAttackTParam = new AudioParameterFloat("legatoAttackT","LegatoAttackT",0.0f,20.0f,0.005f));

    addParameter( pluginParameters.keySwitchRangeShiftParam = new AudioParameterInt( "ksRangeShift","KeyswitchRangeShift",-127,127,0));
    addParameter( pluginParameters.playRangeShiftParam = new AudioParameterInt( "plRangeShift","PlayRangeShift",-127,127,0));
    
    addParameter( pluginParameters.monoModeParam = new AudioParameterBool( "monoMode", "MonoMode", false));

    addParameter( pluginParameters.soundSetParam = new AudioParameterInt("soundSet","SoundSet",0,127,0));
    
    addParameter( pluginParameters.dfdParam = new AudioParameterBool( "dfd","DFD", false));
    addParameter( pluginParameters.dfdPreloadFrames = new AudioParameterInt("dfdPreloadFrames","DFDPreloadFrames",1024,88200,44100));
    addParameter( pluginParameters.dfdLoadFrames = new AudioParameterInt("dfdLoadFrames","DFDLoadFrames",1024,44100,22050));
    addParameter( pluginParameters.dfdLoadAtFramesLeft = new AudioParameterInt("dfdLoadAtFramesLeft","DFDLoadAtFramesLeft",512,44100,11025));

    
}






void SimpleSamplerComponentAudioProcessor::setPatch( PatchDefinition& aPatchDefinition)
{
    //if the desired patchis already loaded, do nothing
    if( currentPatchDefinition != NULL && currentPatchDefinition->getName() == aPatchDefinition.getName())
        return;
    
	sampler.killAllNotes(0);
    sampler.clearSounds();
    sampler.setPatchDefinition(NULL);
    currentPatchDefinition = NULL;
    bool soundsLoaded = true;
    std::string soundsNotLoadedStr = "Samples not loaded:\n\n";
    if( sampleBin.isReady()) {
        
        try {
            sampleBin.resetAudioBuffers();
            sampleBin.setupDfd(   aPatchDefinition.getDfd(),
                                    aPatchDefinition.getNrOfRingBuffers(),
                                    aPatchDefinition.getDfdPreloadFrames(),
                                    aPatchDefinition.getDfdLoadFrames(),
                                    aPatchDefinition.getDfdLoadAtFramesLeft() );
        
        
            for( int i=0; i<aPatchDefinition.getNrOfSoundDefinitions(); i++)
            {
                SoundDefinition* sd = aPatchDefinition.getSoundDefinitionAt(i);
                XFadeSamplerSound* xFadeSamplerSound = new XFadeSamplerSound(sd,&sampleBin,soundsNotLoadedStr);
                if( xFadeSamplerSound->isReady()) {
                    sampler.addSound (xFadeSamplerSound);
                } else {
                    delete xFadeSamplerSound;
                    soundsLoaded = false;
                }
            }
            if( soundsLoaded ) {
                // Preset the parameters
                AHDSREnvelopeDefinition& ahdsrDef = aPatchDefinition.getEnvelopeDefinition();
                
                *pluginParameters.dfdParam = aPatchDefinition.getDfd();
                *pluginParameters.dfdPreloadFrames = aPatchDefinition.getDfdPreloadFrames();
                *pluginParameters.dfdLoadFrames = aPatchDefinition.getDfdLoadFrames();
                
                
                *pluginParameters.gainParam = Decibels::decibelsToGain(aPatchDefinition.getGainDb());
                *pluginParameters.panParam = aPatchDefinition.getPan();
                *pluginParameters.tuneStParam = aPatchDefinition.getTuneSt();
                *pluginParameters.tuneParam = aPatchDefinition.getTune();
                *pluginParameters.veloSensParam = aPatchDefinition.getVeloSens();
                
                for( int i=0; i<MAX_NR_OF_LAYERS; i++ ) {
                    *pluginParameters.layerGainParams[i] = Decibels::decibelsToGain(aPatchDefinition.getLayerGainDb(i));
                }
                
				int nkg = aPatchDefinition.getKeyGroupDefinitions().size();
				if( nkg > MAX_NR_OF_KEYGROUPS ) nkg = MAX_NR_OF_KEYGROUPS;
				for( int i=0; i<nkg; i++ ) {
					*pluginParameters.kgMonoParamsVec[i] = aPatchDefinition.getKeyGroupDefinitions()[i].isMono;
					*pluginParameters.kgGainParamsVec[i] = Decibels::decibelsToGain(aPatchDefinition.getKeyGroupDefinitions()[i].gainDb);
					*pluginParameters.kgPanParamsVec[i] = aPatchDefinition.getKeyGroupDefinitions()[i].pan;
					*pluginParameters.kgTuneStParamsVec[i] = aPatchDefinition.getKeyGroupDefinitions()[i].tuneSt;
					*pluginParameters.kgTuneParamsVec[i] = aPatchDefinition.getKeyGroupDefinitions()[i].tuneCt;
					*pluginParameters.kgBevelParamsVec[i] = aPatchDefinition.getKeyGroupDefinitions()[i].bevel;
				}
				
				
                *pluginParameters.offsetTParam = ahdsrDef.offsetT;
                *pluginParameters.attackTParam = ahdsrDef.attackT;
                *pluginParameters.holdTParam = ahdsrDef.holdT;
                *pluginParameters.decayTParam = ahdsrDef.decayT;
                *pluginParameters.sustainGainParam = Decibels::decibelsToGain(ahdsrDef.sustainDb);
                *pluginParameters.releaseTParam = ahdsrDef.releaseT;
                
                
                *pluginParameters.pressureModeParam = aPatchDefinition.getPressureMode();
                *pluginParameters.pressureControllerParam = aPatchDefinition.getPressureController();
                
                *pluginParameters.pitchWheelModeParam = aPatchDefinition.getPitchWheelMode();
                *pluginParameters.pitchWheelRangeParam = aPatchDefinition.getPitchWheelRangeSt();
                *pluginParameters.pitchWheelRetriggerReleaseTParam = aPatchDefinition.getPitchWheelRetriggerReleaseS();
                *pluginParameters.pitchWheelRetriggerOffsetTParam = aPatchDefinition.getPitchWheelRetriggerOffsetS();
                *pluginParameters.pitchWheelRetriggerAttackTParam = aPatchDefinition.getPitchWheelRetriggerAttackS();
                
                *pluginParameters.legatoModeParam = aPatchDefinition.getLegatoMode();
                *pluginParameters.legatoReleaseTParam = aPatchDefinition.getLegatoReleaseS();
                *pluginParameters.legatoOffsetTParam = aPatchDefinition.getLegatoOffsetS();
                *pluginParameters.legatoAttackTParam = aPatchDefinition.getLegatoAttackS();
                
                *pluginParameters.keySwitchRangeShiftParam = 0;
                *pluginParameters.playRangeShiftParam = 0;
                
                *pluginParameters.monoModeParam = aPatchDefinition.isMonoMode();
                
                
                sampler.setPatchDefinition(&aPatchDefinition);
                
                sampler.clearVoices();
                // Add some voices...
                for (int i = 0; i<aPatchDefinition.getNumberOfVoices(); i++) {
                    sampler.addVoice (new XFadeSamplerVoice(&pluginParameters,&sampleBin, dfdThreadPool));
                }
                
                currentPatchDefinition = &aPatchDefinition;
                
            } else {
                AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, "SampleBin is corrupted", soundsNotLoadedStr, "OK", NULL,NULL);
                
            }
            
        } catch(LoadingError error)
        {
            // print the error message if something went wrong
            //DBG("Error loading: " + error.fileName + ": " + error.errorDescription);
        
            AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, error.fileName, error.errorDescription, "OK", NULL,NULL);
        
            jassertfalse;
            return;
        } catch(std::bad_alloc memoryExeption)
        {
            AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, "", "Out of memory", "OK", NULL,NULL);
        }
    
    } else {
        AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, "SampleBin", "Bin not ready setting patch.", "OK", NULL,NULL);
    }
    
}


PatchDefinition* SimpleSamplerComponentAudioProcessor::getCurrentPatchDefinition()
{
    return currentPatchDefinition;
}



/*
PatchDefinition* SimpleSamplerComponentAudioProcessor::findPatchDefinition( std::string aName)
{
    PatchDefinition* pd = NULL;
    for( int i=0; i<(int)patchBank.size(); i++) {
        if( patchBank.at(i)->getName() == aName ) {
            pd = patchBank.at(i);
            break;
        }
    }
    return pd;
}

int SimpleSamplerComponentAudioProcessor::findPatchIndex( std::string aName)
{
    for( int i=0; i<(int)patchBank.size(); i++) {
        if( patchBank.at(i)->getName() == aName ) {
            return i;
        }
    }
    return -1;
}
*/



const String SimpleSamplerComponentAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool SimpleSamplerComponentAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SimpleSamplerComponentAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double SimpleSamplerComponentAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SimpleSamplerComponentAudioProcessor::getNumPrograms()
{
    return patchBank.getNrOfPatchDefinitions();


    //return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int SimpleSamplerComponentAudioProcessor::getCurrentProgram()
{
    return currentPatchNdx;
}

void SimpleSamplerComponentAudioProcessor::setCurrentProgram (int index)
{
    if( index > -1 && currentPatchNdx != index) {
        PatchDefinition* pd = patchBank.getPatchDefinitionAt(index);
        //PatchDefinition* pd = patchBank.at(index);
        try {
            setPatch(*pd);

            if( editor != nullptr) {
                //const MessageManagerLock mml (Thread::getCurrentThread());

                //if (! mml.lockWasGained())  // if something is trying to kill this job, the lock
                //	return;                 // will fail, in which case we'd better return..

                editor->setPatchDefinition(currentPatchDefinition);

            }
            currentPatchNdx = index;
            *pluginParameters.soundSetParam = 0;
        } catch(LoadingError error)
        {
            // print the error message if something went wrong
            //DBG("Error loading: " + error.fileName + ": " + error.errorDescription);
        
            AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, error.fileName, error.errorDescription, "OK", NULL,NULL);
        
            jassertfalse;
            return;
        } catch(std::bad_alloc memoryExeption)
        {
            AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::WarningIcon, "Out of memory", "Try to free memory and reload", "OK", NULL,NULL);
        }
    }
}

const String SimpleSamplerComponentAudioProcessor::getProgramName (int index)
{
    String aProgName;
    if( index > -1) {
        PatchDefinition* pd = patchBank.getPatchDefinitionAt(index);
        if( pd != nullptr) {
            aProgName = pd->getName();
        }
    }
    return aProgName;
}

void SimpleSamplerComponentAudioProcessor::changeProgramName (int /*index*/, const String& /*newName*/)
{
}

//==============================================================================
void SimpleSamplerComponentAudioProcessor::prepareToPlay (double sampleRate, int /*samplesPerBlock*/)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    sampler.setCurrentPlaybackSampleRate (sampleRate);
    keyboardState.reset();
}

void SimpleSamplerComponentAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
    keyboardState.reset();
    
}
#ifndef JucePlugin_PreferredChannelConfigurations
bool SimpleSamplerComponentAudioProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
#if JucePlugin_IsMidiEffect
	ignoreUnused(layouts);
	return true;
#else
	// This is the place where you check if the layout is supported.
	// In this template code we only support mono or stereo.
	if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
		&& layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
		return false;

	// This checks if the input layout matches the output layout
#if ! JucePlugin_IsSynth
	if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
		return false;
#endif

	return true;
#endif
}
#endif
/*
void SimpleSamplerComponentAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        float* channelData = buffer.getWritePointer (channel);

        // ..do something to the data...
    }
}
*/

template <typename FloatType>
void SimpleSamplerComponentAudioProcessor::process (AudioBuffer<FloatType>& buffer,
                                            MidiBuffer& midiMessages)
{
    const int numSamples = buffer.getNumSamples();
    
    // Now pass any incoming midi messages to our keyboard state object, and let it
    // add messages to the buffer if the user is clicking on the on-screen keys
    keyboardState.processNextMidiBuffer (midiMessages, 0, numSamples, true);
    
    // and now get our synth to process these midi events and generate its output.
    sampler.renderNextBlock (buffer, midiMessages, 0, numSamples);
    
    
    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    //for (int i = getTotalNumInputChannels(); i < getTotalNumOutputChannels(); ++i)
    //    buffer.clear (i, 0, numSamples);
    
}


//==============================================================================
bool SimpleSamplerComponentAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* SimpleSamplerComponentAudioProcessor::createEditor()
{
    editor = new SimpleSamplerComponentAudioProcessorEditor (*this, &uiConfig);
    sampler.setEditor(editor);
    editor->setBackgroundImg(&sampleBin.getBackgroundImage());
    /*editor->setRotarySliderImg(&sampleBin.getRotarySliderImage(), uiConfigurations.rotarySliderWidth, uiConfigurations.rotarySliderHeight);
    editor->setToggleButtonImage(&sampleBin.getToggleButtonImage(), uiConfigurations.toggleButtonWidth, uiConfigurations.toggleButtonHeight);
    editor->setKnobImages(<#std::map<std::string, juce::Image> *aKnobImages#>)*/
    editor->setPatchDefinition(currentPatchDefinition);
    return editor;
}

//==============================================================================
void SimpleSamplerComponentAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // Here's an example of how you can use XML to make it easy and more robust:
    
    // Create an outer XML element..
    XmlElement xml ("MYPLUGINSETTINGS");
    if( currentPatchDefinition != NULL ) {
        std::string patchName = currentPatchDefinition->getName();
        xml.setAttribute ("patch",patchName);
    }
    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i)
        if (AudioProcessorParameterWithID* p = dynamic_cast<AudioProcessorParameterWithID*> (getParameters().getUnchecked(i)))
            xml.setAttribute (p->paramID, p->getValue());
    
    // then use this helper function to stuff it into the binary blob and return it..
    copyXmlToBinary (xml, destData);
}

void SimpleSamplerComponentAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    
    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));
    
    if (xmlState != nullptr)
        {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName ("MYPLUGINSETTINGS"))
            {
            const juce::String& aPatchName = xmlState->getStringAttribute("patch");
            if( aPatchName != "" && (currentPatchDefinition==NULL || currentPatchDefinition->getName() != aPatchName)) {
                //int ndx = findPatchIndex(aPatchName.toStdString());
                std::string aName = aPatchName.toStdString();
                int ndx =  patchBank.getPatchDefinitionNdx(aName);
                if( ndx > -1) {
                    setCurrentProgram(ndx);
                } else {
                    return;
                }
            }
            
            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID* p = dynamic_cast<AudioProcessorParameterWithID*> (getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost ((float) xmlState->getDoubleAttribute (p->paramID, p->getValue()));
            }
        }
}



StringArray SimpleSamplerComponentAudioProcessor::getControllersStringArray()
{
	StringArray clist;
	clist.add("000-Bank Select");
	clist.add("001-Modulation Wheel");
	clist.add("002-Breath Contoller");
	clist.add("003-Undefined");
	clist.add("004-Foot Controller");
	clist.add("005-Portamento Time");
	clist.add("006-Data Entry MSB");
	clist.add("007-Main Volume");
	clist.add("008-Balance");
	clist.add("009-Undefined");
	clist.add("010-Pan");
	clist.add("011-0Ch");
	clist.add("012-Effect Control 1");
	clist.add("013-Effect Control 2");
	clist.add("014-Undefined");
	clist.add("015-Undefined");
	clist.add("016-General Purpose");
	clist.add("017-General Purpose");
	clist.add("018-General Purpose");
	clist.add("019-General Purpose");
	clist.add("020-Undefined");
	clist.add("021-Undefined");
	clist.add("022-Undefined");
	clist.add("023-Undefined");
	clist.add("024-Undefined");
	clist.add("025-Undefined");
	clist.add("026-Undefined");
	clist.add("027-Undefined");
	clist.add("028-Undefined");
	clist.add("029-Undefined");
	clist.add("030-Undefined");
	clist.add("031-Undefined");
	clist.add("032-LSB for Controller 000");
	clist.add("033-LSB for Controller 001");
	clist.add("034-LSB for Controller 002");
	clist.add("035-LSB for Controller 003");
	clist.add("036-LSB for Controller 004");
	clist.add("037-LSB for Controller 005");
	clist.add("038-LSB for Controller 006");
	clist.add("039-LSB for Controller 007");
	clist.add("040-LSB for Controller 008");
	clist.add("041-LSB for Controller 009");
	clist.add("042-LSB for Controller 010");
	clist.add("043-LSB for Controller 011");
	clist.add("044-LSB for Controller 012");
	clist.add("045-LSB for Controller 013");
	clist.add("046-LSB for Controller 014");
	clist.add("047-LSB for Controller 015");
	clist.add("048-LSB for Controller 016");
	clist.add("049-LSB for Controller 017");
	clist.add("050-LSB for Controller 018");
	clist.add("051-LSB for Controller 019");
	clist.add("052-LSB for Controller 020");
	clist.add("053-LSB for Controller 021");
	clist.add("054-LSB for Controller 022");
	clist.add("055-LSB for Controller 023");
	clist.add("056-LSB for Controller 024");
	clist.add("057-LSB for Controller 025");
	clist.add("058-LSB for Controller 026");
	clist.add("059-LSB for Controller 027");
	clist.add("060-LSB for Controller 028");
	clist.add("061-LSB for Controller 029");
	clist.add("062-LSB for Controller 030");
	clist.add("063-LSB for Controller 031");
	clist.add("064-Damper Pedat (Sustain)");
	clist.add("065-Portamento");
	clist.add("066-Sostenuto");
	clist.add("067-Soft Pedal");
	clist.add("068-Legato Footswitch");
	clist.add("069-Hold 2");
	clist.add("070-Sound Controller 1");
	clist.add("071-Sound Controller 2");
	clist.add("072-Sound Controller 3");
	clist.add("073-Sound Controller 4");
	clist.add("074-Sound Controller 5");
	clist.add("075-Sound Controller 6");
	clist.add("076-Sound Controller 7");
	clist.add("077-Sound Controller 8");
	clist.add("078-Sound Controller 9");
	clist.add("079-Sound Controller 10");
	clist.add("080-General Purpose");
	clist.add("081-General Purpose");
	clist.add("082-General Purpose");
	clist.add("083-General Purpose");
	clist.add("084-Portamento Control");
	clist.add("085-Undefined");
	clist.add("086-Undefined");
	clist.add("087-Undefined");
	clist.add("088-Undefined");
	clist.add("089-Undefined");
	clist.add("090-Undefined");
	clist.add("091-Effects 1 Depth");
	clist.add("092-Effects 2 Depth");
	clist.add("093-Effects 3 Depth");
	clist.add("094-Effects 4 Depth");
	clist.add("095-Effects 5 Depth");
	clist.add("096-Data Increment");
	clist.add("097-Data Decrement");
	clist.add("098-Non-Registered Parameter Number LSB");
	clist.add("099-Non-Registered Parameter Number MSB");
	clist.add("100-Registered Parameter Number LSB");
	clist.add("101-Registered Parameter Number MSB");
	clist.add("102-Undefined");
	clist.add("103-Undefined");
	clist.add("104-Undefined");
	clist.add("105-Undefined");
	clist.add("106-Undefined");
	clist.add("107-Undefined");
	clist.add("108-Undefined");
	clist.add("109-Undefined");
	clist.add("110-Undefined");
	clist.add("111-Undefined");
	clist.add("112-Undefined");
	clist.add("113-Undefined");
	clist.add("114-Undefined");
	clist.add("115-Undefined");
	clist.add("116-Undefined");
	clist.add("117-Undefined");
	clist.add("118-Undefined");
	clist.add("119-Undefined");
	clist.add("120-Undefined");
	clist.add("121-Reset All Controllers");
	clist.add("122-Local Control");
	clist.add("123-All Notes Off");
	clist.add("124-Omni Off");
	clist.add("125-Omni On");
	clist.add("126-Mono On (Poly Off)");
	clist.add("127-Poly On (Mono Off)");
	return clist;
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SimpleSamplerComponentAudioProcessor();
}










