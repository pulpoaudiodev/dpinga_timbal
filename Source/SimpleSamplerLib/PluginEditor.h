/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "UIControls.hpp"
#include "PatchDefinition.hpp"
#include "../ProductDef.h"



//==============================================================================
/**
*/
class SimpleSamplerComponentAudioProcessorEditor  : public AudioProcessorEditor,
													private Button::Listener
{
public:
    SimpleSamplerComponentAudioProcessorEditor (SimpleSamplerComponentAudioProcessor&, UIConfig* aUIConfig);
    ~SimpleSamplerComponentAudioProcessorEditor();

    //==============================================================================
	
    void setBackgroundImg( juce::Image* aBackgroundImg);
    void removeUIElements();
    void createUIElements();
	void recreateUIElements(UIConfig* aUIConfig);
	
    void paint (Graphics&) override;
    void resized() override;
    void configureComponents();

    void setPatchDefinition(PatchDefinition* aPatchDefinition);
	
    void addLabel( Label* aLabel, std::string aName);
    void configureLabel( Label* aLabel, std::string aName);
    Label* createAndConfigureLabel( std::string aName);
    void addSlider( ParameterSlider* aSlider, std::string aName);
    void configureSlider( ParameterSlider* aSlider, std::string aName);
    void addAndConfigureSlider( ParameterSlider* aSlider, std::string aName);
    void addComboBox( ComboBox* aCombo, std::string aName);
    void configureComboBox( ComboBox* aCombo, std::string aName);
    void addAndConfigureComboBox( ComboBox* aCombo, std::string aName);
    void addButton( Button* aButton, std::string aName);
    void configureButton( Button* aButton, std::string aName);
    void addAndConfigureButton( Button* aButton, std::string aName);
    void addComponent( Component* aComponent, std::string aName);
    void configureComponent( Component* aComponent, std::string aName);
    void addAndConfigureComponent( Component* aComponent, std::string aName);
    
private:
    UIUpdateTimer uiUpdateTimer;
    juce::Image* backgroundImg;
    
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    SimpleSamplerComponentAudioProcessor& processor;

    PatchDefinition* patchDefinition;
    UIConfig* uiConfig;

    ScopedPointer<Label> gainLabel, panLabel, tuneStLabel, tuneLabel, veloSensLabel, bevelLabel;
	ScopedPointer<Label> offsetTLabel, attackTLabel, holdTLabel, decayTLabel, sustainDbLabel, releaseTLabel;
	ScopedPointer<Label> layer1Label, layer2Label, layer3Label, layer4Label;
    ScopedPointer<Label> pressureModeLabel, pressureControllerLabel;
	ScopedPointer<Label> pitchWheelModeLabel, pitchWheelRangeLabel, pitchWheelReleaseTLabel, pitchWheelOffsetTLabel, pitchWheelAttackTLabel;
	ScopedPointer<Label> legatoModeLabel, legatoReleaseTLabel,legatoOffsetTLabel,legatoAttackTLabel;
	ScopedPointer<Label> keySwitchRangeShiftlabel, playRangeShiftLabel, monoModeLabel, dfdLabel;
	ScopedPointer<Label> soundSetLabel;
    
	ScopedPointer<TextButton> reloadPatchesButton;
    ScopedPointer<TextButton> reloadUIButton;
    ScopedPointer<TextButton> aboutButton;
    
    ScopedPointer<ParameterKeyRangeMidiKeyboardComponent> midiKeyboard;
    
    ScopedPointer<DBParameterSlider> gainSlider;
    ScopedPointer<PrcParameterSlider> panSlider;
    ScopedPointer<ParameterIntSlider> tuneStSlider;
    ScopedPointer<ParameterSlider> tuneSlider;
    ScopedPointer<ParameterSlider> veloSensSlider;
    ScopedPointer<ParameterSlider> bevelSlider;
    
    ScopedPointer<ParameterSlider> offsetTSlider;
    ScopedPointer<ParameterSlider> attackTSlider;
    ScopedPointer<ParameterSlider> holdTSlider;
    ScopedPointer<ParameterSlider> decayTSlider;
    ScopedPointer<DBParameterSlider> sustainDbSlider;
    ScopedPointer<ParameterSlider> releaseTSlider;
    
    /*
    std::vector<ScopedPointer<DBParameterSlider>> layerGainSliders;


    std::vector<ScopedPointer<ParameterToggleButton>> kgMonoToggleButtonVec;
    std::vector<ScopedPointer<DBParameterSlider>> kgGainSliderVec;
    std::vector<ScopedPointer<PrcParameterSlider>> kgPanSliderVec;
    std::vector<ScopedPointer<ParameterIntSlider>> kgTuneStSliderVec;
    std::vector<ScopedPointer<ParameterSlider>> kgTuneSliderVec;
    std::vector<ScopedPointer<ParameterSlider>> kgBevelSliderVec;
    */


    std::vector<DBParameterSlider*> layerGainSliders;
    
    
    std::vector<ParameterToggleButton*> kgMonoToggleButtonVec;
    std::vector<DBParameterSlider*> kgGainSliderVec;
    std::vector<PrcParameterSlider*> kgPanSliderVec;
    std::vector<ParameterIntSlider*> kgTuneStSliderVec;
    std::vector<ParameterSlider*> kgTuneSliderVec;
    std::vector<ParameterSlider*> kgBevelSliderVec;
    
    
    ScopedPointer<ParameterChoiceComboBox> pressureModeCombo;
    ScopedPointer<ParameterChoiceComboBox> pressureControllerCombo;

    ScopedPointer<ParameterChoiceComboBox> pitchWheelModeCombo;
    ScopedPointer<ParameterIntSlider> pitchWheelRangeSlider;
    ScopedPointer<ParameterSlider> pitchWheelRetriggerReleaseTSlider;
    ScopedPointer<ParameterSlider> pitchWheelRetriggerOffsetTSlider;
    ScopedPointer<ParameterSlider> pitchWheelRetriggerAttackTSlider;

    ScopedPointer<ParameterChoiceComboBox> legatoModeCombo;
    ScopedPointer<ParameterSlider> legatoReleaseTSlider;
    ScopedPointer<ParameterSlider> legatoOffsetTSlider;
    ScopedPointer<ParameterSlider> legatoAttackTSlider;

    ScopedPointer<ParameterIntSlider> keySwitchRangeShiftSlider;
    ScopedPointer<ParameterIntSlider> playRangeShiftSlider;
    
    ScopedPointer<ParameterToggleButton> monoModeToggleButton;
    ScopedPointer<ParameterToggleButton> dfdToggleButton;
    
    ScopedPointer<ParameterIntComboBox> soundSetCombo;

    
    virtual void buttonClicked (Button* buttonThatWasClicked) override;

    void updateUIControls();
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleSamplerComponentAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
