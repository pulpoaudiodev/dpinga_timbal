/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  SamplerErrors.hpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 30/12/16.
//
//

#ifndef SamplerErrors_h
#define SamplerErrors_h

#include "JuceHeader.h"


/** An object of this class will be thrown if the loading of the sound fails.
 */
struct LoadingError
{
    /** Create one of this, if the sound fails to load.
     *
     *	@param fileName the file that caused the error.
     *	@param errorDescription a description of what went wrong.
     */
    LoadingError(const String &fileName_, const String &errorDescription_):
    fileName(fileName_),
    errorDescription(errorDescription_)
    {};
    
    String fileName;
    String errorDescription;
};

#endif /* SamplerErrors_h */
