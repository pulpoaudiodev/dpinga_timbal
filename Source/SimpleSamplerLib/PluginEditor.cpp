/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include <sstream>

#include "PluginProcessor.h"
#include "PluginEditor.h"
//#include "../UIImages.h"



//==============================================================================
// This is a handy slider subclass that controls an AudioProcessorParameter
// (may move this class into the library itself at some point in the future..)



//==============================================================================
SimpleSamplerComponentAudioProcessorEditor::SimpleSamplerComponentAudioProcessorEditor (SimpleSamplerComponentAudioProcessor& p, UIConfig* aUiConfig)
    : AudioProcessorEditor (&p), processor (p)

{
    patchDefinition = NULL;
    uiConfig = aUiConfig;
    backgroundImg = nullptr;
  
    
    setSize (aUiConfig->w, aUiConfig->h);
	
	createUIElements();
	
}





SimpleSamplerComponentAudioProcessorEditor::~SimpleSamplerComponentAudioProcessorEditor()
{
}



void SimpleSamplerComponentAudioProcessorEditor::setBackgroundImg( juce::Image* aBackgroundImg)
{
    backgroundImg = aBackgroundImg;
}




//==============================================================================


void SimpleSamplerComponentAudioProcessorEditor::removeUIElements()
{
    // remove of all UI elements has to be called seperately from outside
    // BEFORE uiConfig  (is a pointer and gets invalid from Processor when reloading UI)
    // is deleted !!!
    removeAllChildren();
    layerGainSliders.clear();
    kgMonoToggleButtonVec.clear();
    kgGainSliderVec.clear();
    kgPanSliderVec.clear();
    kgTuneStSliderVec.clear();
    kgTuneSliderVec.clear();
    kgBevelSliderVec.clear();
}

void SimpleSamplerComponentAudioProcessorEditor::recreateUIElements(UIConfig* aUIConfig)
{
    uiConfig = aUIConfig;
    createUIElements();
}

void SimpleSamplerComponentAudioProcessorEditor::createUIElements()
{

    addChildComponent(reloadPatchesButton = new TextButton("Rel.Conf"));
    reloadPatchesButton->addListener(this);
    
    if( uiConfig->showReloadPatchesButton) {
        reloadPatchesButton->setBounds(0,0,70,20);
        reloadPatchesButton->setVisible(true);
    } else {
        reloadPatchesButton->setVisible(false);
    }

    addChildComponent(reloadUIButton=new TextButton("Rel.UI"));
    reloadUIButton->addListener(this);

    if( uiConfig->showReloadUIButton) {
        reloadUIButton->setBounds(70,0,70,20);
        reloadUIButton->setVisible(true);
    } else {
        reloadUIButton->setVisible(false);
    }
    
    
    
    
    gainLabel = createAndConfigureLabel( "LbGain");
    panLabel= createAndConfigureLabel( "LbPan");
    tuneStLabel= createAndConfigureLabel( "LbTuneSt");
    tuneLabel= createAndConfigureLabel( "LbTune");
    veloSensLabel= createAndConfigureLabel( "LbVelSens");
    //bevelLabel= createAndConfigureLabel( "Bevel");
    offsetTLabel= createAndConfigureLabel( "LbEnvOffset");
    attackTLabel= createAndConfigureLabel( "LbEnvAttack");
    holdTLabel= createAndConfigureLabel( "LbEnvHold");
    decayTLabel= createAndConfigureLabel( "LbEnvDecay");
    sustainDbLabel= createAndConfigureLabel( "LbEnvSustain");
    releaseTLabel= createAndConfigureLabel( "LbEnvRelease");
    layer1Label= createAndConfigureLabel( "LbLayer1Gain");
    layer2Label= createAndConfigureLabel( "LbLayer2Gain");
    layer3Label= createAndConfigureLabel( "LbLayer3Gain");
    layer4Label= createAndConfigureLabel( "LbLayer4Gain");
    pressureModeLabel= createAndConfigureLabel( "LbPressureMode");
    pressureControllerLabel= createAndConfigureLabel( "LbPressureController");
    pitchWheelModeLabel= createAndConfigureLabel( "LbPitchWheelMode");
    pitchWheelRangeLabel= createAndConfigureLabel( "LbPitchWheelRange");
    pitchWheelReleaseTLabel= createAndConfigureLabel( "LbPitchWheelRelease");
    pitchWheelOffsetTLabel= createAndConfigureLabel( "LbPitchWheelOffset");
    pitchWheelAttackTLabel= createAndConfigureLabel( "LbPitchWheelAttack");
    legatoModeLabel= createAndConfigureLabel( "LbLegatoMode");
    legatoReleaseTLabel= createAndConfigureLabel( "LbLegatoRelease");
    legatoOffsetTLabel= createAndConfigureLabel( "LbLegatoOffset");
    legatoAttackTLabel= createAndConfigureLabel( "LbLegatoAttack");
    keySwitchRangeShiftlabel= createAndConfigureLabel( "LbKeySwitchRangeShift");
    playRangeShiftLabel= createAndConfigureLabel( "LbPlayRangeShift");
	monoModeLabel= createAndConfigureLabel( "LbMonoMode");
    dfdLabel= createAndConfigureLabel( "LbDfd");
    soundSetLabel= createAndConfigureLabel( "LbSoundSet");

	SimpleSamplerComponentAudioProcessor& p = *((SimpleSamplerComponentAudioProcessor*) getAudioProcessor());



    if( uiConfig->getElementConfig("BuAbout")) {
        addAndConfigureButton(aboutButton = new TextButton(""), "BuAbout");
        aboutButton->addListener(this);
    }
    
    if( uiConfig->getElementConfig("CoMidiKeyboard") ) addAndConfigureComponent( midiKeyboard = new ParameterKeyRangeMidiKeyboardComponent(&uiUpdateTimer, p.keyboardState, MidiKeyboardComponent::horizontalKeyboard,*p.getPluginParameters()->playRangeShiftParam, *p.getPluginParameters()->keySwitchRangeShiftParam), "CoMidiKeyboard");
    
    if( uiConfig->getElementConfig("SlPlayRangeShift") ) addAndConfigureSlider( keySwitchRangeShiftSlider= new ParameterIntSlider (&uiUpdateTimer, *p.getPluginParameters()->keySwitchRangeShiftParam, " st",1.0f),"SlPlayRangeShift");
    if( uiConfig->getElementConfig("SlKeySwitchRangeShift") ) addAndConfigureSlider( playRangeShiftSlider= new ParameterIntSlider (&uiUpdateTimer, *p.getPluginParameters()->playRangeShiftParam, " st",1.0f),"SlKeySwitchRangeShift");
    if( uiConfig->getElementConfig("BuMonoMode") ) addAndConfigureButton( monoModeToggleButton = new ParameterToggleButton(&uiUpdateTimer, *p.getPluginParameters()->monoModeParam), "BuMonoMode");
    if( uiConfig->getElementConfig("BuDfd") ) addAndConfigureButton( dfdToggleButton = new ParameterToggleButton(&uiUpdateTimer, *p.getPluginParameters()->dfdParam), "BuDfd");
    if( uiConfig->getElementConfig("CbSoundSet") ) addAndConfigureComboBox(soundSetCombo = new ParameterIntComboBox (&uiUpdateTimer, *p.getPluginParameters()->soundSetParam), "CbSoundSet");

    
    if( uiConfig->getElementConfig("SlGain") ) addAndConfigureSlider( gainSlider = new DBParameterSlider (&uiUpdateTimer, *p.getPluginParameters()->gainParam, 0.25f),"SlGain");
    if( uiConfig->getElementConfig("SlPan") ) addAndConfigureSlider( panSlider = new PrcParameterSlider (&uiUpdateTimer, *p.getPluginParameters()->panParam, 1.0f),"SlPan");
    if( uiConfig->getElementConfig("SlTune") ) addAndConfigureSlider(tuneSlider = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->tuneParam, " st", 1.0f),"SlTune");
    if( uiConfig->getElementConfig("SlTuneSt") ) addAndConfigureSlider(tuneStSlider = new ParameterIntSlider (&uiUpdateTimer, *p.getPluginParameters()->tuneStParam, " st", 1.0f),"SlTuneSt");
    if( uiConfig->getElementConfig("SlVelSens") ) addAndConfigureSlider( veloSensSlider = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->veloSensParam, "", 1.0f),"SlVelSens");
    
    if( uiConfig->getElementConfig("CbPressureMode") ) addAndConfigureComboBox( pressureModeCombo = new ParameterChoiceComboBox (&uiUpdateTimer, *p.getPluginParameters()->pressureModeParam), "CbPressureMode");
    if( uiConfig->getElementConfig("CbPressureController") ) addAndConfigureComboBox( pressureControllerCombo = new ParameterChoiceComboBox (&uiUpdateTimer, *p.getPluginParameters()->pressureControllerParam), "CbPressureController");

    
    for( int i=0; i<MAX_NR_OF_LAYERS; i++) {
        std::stringstream paramLayerGainName; paramLayerGainName << "SlLayer" << i << "Gain";
        if( uiConfig->getElementConfig(paramLayerGainName.str())) {
            DBParameterSlider* slLayerGain = new DBParameterSlider (&uiUpdateTimer, *p.getPluginParameters()->layerGainParams[i], 0.25f);
            layerGainSliders.push_back( slLayerGain);
            addAndConfigureSlider(slLayerGain, paramLayerGainName.str());
        }
    }
    
    

    if( uiConfig->getElementConfig("SlEnvOffset") ) addAndConfigureSlider( offsetTSlider = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->offsetTParam," s", 0.25f) ,"SlEnvOffset");
    if( uiConfig->getElementConfig("SlEnvAttack") ) addAndConfigureSlider( attackTSlider = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->attackTParam," s", 0.25f) ,"SlEnvAttack");
    if( uiConfig->getElementConfig("SlEnvHold") ) addAndConfigureSlider( holdTSlider = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->holdTParam, " s", 0.25f) ,"SlEnvHold");
    if( uiConfig->getElementConfig("SlEnvDecay") ) addAndConfigureSlider( decayTSlider = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->decayTParam, " s", 0.25f) ,"SlEnvDecay");
    if( uiConfig->getElementConfig("SlEnvSustain") ) addAndConfigureSlider(sustainDbSlider = new DBParameterSlider (&uiUpdateTimer, *p.getPluginParameters()->sustainGainParam, 0.25f) ,"SlEnvSustain");
    if( uiConfig->getElementConfig("SlEnvRelease") ) addAndConfigureSlider( releaseTSlider = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->releaseTParam, " s", 0.25f) ,"SlEnvRelease");
    
    if( uiConfig->getElementConfig("CbLegatoMode") ) addAndConfigureComboBox(legatoModeCombo= new ParameterChoiceComboBox (&uiUpdateTimer, *p.getPluginParameters()->legatoModeParam), "CbLegatoMode");
    if( uiConfig->getElementConfig("SlLegatoRelease") ) addAndConfigureSlider(legatoReleaseTSlider= new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->legatoReleaseTParam, " s", 0.25f), "SlLegatoRelease");
    if( uiConfig->getElementConfig("SlLegatoOffset") ) addAndConfigureSlider(legatoOffsetTSlider= new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->legatoOffsetTParam, " s", 0.25f), "SlLegatoOffset");
    if( uiConfig->getElementConfig("SlLegatoAttack") ) addAndConfigureSlider(legatoAttackTSlider= new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->legatoAttackTParam, " s", 0.25f), "SlLegatoAttack");
    
    if( uiConfig->getElementConfig("CbPitchWheelMode") ) addAndConfigureComboBox(pitchWheelModeCombo = new ParameterChoiceComboBox (&uiUpdateTimer, *p.getPluginParameters()->pitchWheelModeParam), "CbPitchWheelMode");
    if( uiConfig->getElementConfig("SlPitchWheelRange") ) addAndConfigureSlider(pitchWheelRangeSlider = new ParameterIntSlider (&uiUpdateTimer, *p.getPluginParameters()->pitchWheelRangeParam, " st", 1.0f) ,"SlPitchWheelRange");
    if( uiConfig->getElementConfig("SlPitchWheelRelease") ) addAndConfigureSlider(pitchWheelRetriggerReleaseTSlider= new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->pitchWheelRetriggerReleaseTParam, " s", 0.25f) ,"SlPitchWheelRelease");
    if( uiConfig->getElementConfig("SlPitchWheelOffset") ) addAndConfigureSlider(pitchWheelRetriggerOffsetTSlider= new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->pitchWheelRetriggerOffsetTParam, " s", 0.25f) ,"SlPitchWheelOffset");
    if( uiConfig->getElementConfig("SlPitchWheelAttack") ) addAndConfigureSlider(pitchWheelRetriggerAttackTSlider= new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->pitchWheelRetriggerAttackTParam, " s", 0.25f) ,"SlPitchWheelAttack");
    
    
    
    //==================================
    
    for( int i=0; i<MAX_NR_OF_KEYGROUPS; i++) {
    
        std::stringstream paramMonoName; paramMonoName << "BuKg" << i << "Mono";
        if( uiConfig->getElementConfig(paramMonoName.str()) ) {
            ParameterToggleButton* buKgMono = new ParameterToggleButton(&uiUpdateTimer, *p.getPluginParameters()->kgMonoParamsVec[i]);
            kgMonoToggleButtonVec.push_back(buKgMono);
            addAndConfigureButton( buKgMono, paramMonoName.str());
        }
        std::stringstream paramGainName; paramGainName << "SlKg" << i << "GainDb";
        if( uiConfig->getElementConfig(paramGainName.str()) ) {
            DBParameterSlider* slKgGain = new DBParameterSlider (&uiUpdateTimer, *p.getPluginParameters()->kgGainParamsVec[i], 0.25f);
            kgGainSliderVec.push_back(slKgGain);
            addAndConfigureSlider(slKgGain, paramGainName.str());
        }
        std::stringstream paramPanName; paramPanName << "SlKg" << i << "Pan";
        if( uiConfig->getElementConfig(paramPanName.str()) ) {
            PrcParameterSlider* slKgPan = new PrcParameterSlider (&uiUpdateTimer, *p.getPluginParameters()->kgPanParamsVec[i], 1.0f);
            kgPanSliderVec.push_back(slKgPan);
            addAndConfigureSlider(slKgPan, paramPanName.str());
        }
        std::stringstream paramTuneStName; paramTuneStName << "SlKg" << i << "TuneSt";
        if( uiConfig->getElementConfig(paramTuneStName.str()) ) {
            ParameterIntSlider* slKgTuneSt = new ParameterIntSlider (&uiUpdateTimer, *p.getPluginParameters()->kgTuneStParamsVec[i], " st", 1.0f);
            kgTuneStSliderVec.push_back(slKgTuneSt);
            addAndConfigureSlider(slKgTuneSt, paramTuneStName.str());
        }
        std::stringstream paramTuneName; paramTuneName << "SlKg" << i << "Tune";
        if( uiConfig->getElementConfig(paramTuneName.str()) ) {
            ParameterFloatSlider* slKgTuneCt = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->kgTuneParamsVec[i], " st", 1.0f);
            kgTuneSliderVec.push_back(slKgTuneCt);
            addAndConfigureSlider(slKgTuneCt, paramTuneName.str());
        }
        std::stringstream paramBevelName; paramBevelName << "SlKg" << i << "Bevel";
        if( uiConfig->getElementConfig(paramBevelName.str()) ) {
            ParameterFloatSlider* slKgBevel = new ParameterFloatSlider (&uiUpdateTimer, *p.getPluginParameters()->kgBevelParamsVec[i], "", 1.0f);
            kgBevelSliderVec.push_back(slKgBevel);
            addAndConfigureSlider(slKgBevel, paramBevelName.str());
        }
    }
}


void SimpleSamplerComponentAudioProcessorEditor::paint (Graphics& g)
{


    
    if( backgroundImg != NULL && uiConfig->showBackground) {
        g.drawImage( *backgroundImg,0,0,backgroundImg->getWidth(),backgroundImg->getHeight(),0,0,backgroundImg->getWidth(),backgroundImg->getHeight());
    } else {
        g.setColour(juce::Colours::black);
        g.fillRect(0,0,uiConfig->w, uiConfig->h);
    }
}


void SimpleSamplerComponentAudioProcessorEditor::addLabel( Label* aLabel, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL && uec->param != "" ) {
        aLabel->setText(uec->param, NotificationType::dontSendNotification );
		addChildComponent(*aLabel);
	}	
}

void SimpleSamplerComponentAudioProcessorEditor::configureLabel( Label* aLabel, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aLabel->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aLabel->setLookAndFeel(laf);
        }
        aLabel->setJustificationType(juce::Justification::centred);
        aLabel->setVisible(uec->visible);
    }
}

Label* SimpleSamplerComponentAudioProcessorEditor::createAndConfigureLabel( std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL && uec->param != "" ) {
		Label* aLabel = new Label("");
        aLabel->setText(uec->param, NotificationType::dontSendNotification );
		addChildComponent(*aLabel);
        if( uec->hasBounds()) {
            aLabel->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aLabel->setLookAndFeel(laf);
        }
 		aLabel->setJustificationType(juce::Justification::centred);
        aLabel->setVisible(uec->visible);
		return aLabel;
	}	
	return NULL;
}


void SimpleSamplerComponentAudioProcessorEditor::addSlider( ParameterSlider* aSlider, std::string aName)
{
    addChildComponent(aSlider);
    
}

void SimpleSamplerComponentAudioProcessorEditor::configureSlider( ParameterSlider* aSlider, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
        aSlider->enableTextBox(uec->showTextBox);
        if( uec->hasBounds()) {
            aSlider->setBounds( uec->x, uec->y, uec->w, uec->h);
            
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aSlider->setLookAndFeel(laf);
        }
        
        aSlider->setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addAndConfigureSlider( ParameterSlider* aSlider, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
        aSlider->enableTextBox(uec->showTextBox);
        addChildComponent(aSlider);
		if( uec->hasBounds()) {
            aSlider->setBounds( uec->x, uec->y, uec->w, uec->h);
            
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aSlider->setLookAndFeel(laf);
        }
        aSlider->setVisible(uec->visible);
    }
}


void SimpleSamplerComponentAudioProcessorEditor::addComboBox( ComboBox* aCombo, std::string aName)
{
    addChildComponent(aCombo);
    
}

void SimpleSamplerComponentAudioProcessorEditor::configureComboBox( ComboBox* aCombo, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aCombo->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        if( uec->hasSize()) {
            aCombo->setSize(uec->sizeX, uec->sizeY);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aCombo->setLookAndFeel(laf);
        }
        
        aCombo->setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addAndConfigureComboBox( ComboBox* aComboBox, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
		addChildComponent(aComboBox);
		if( uec->hasBounds()) {
            aComboBox->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        if( uec->hasSize()) {
            aComboBox->setSize(uec->sizeX, uec->sizeY);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aComboBox->setLookAndFeel(laf);
        }
        aComboBox->setVisible(uec->visible);
    }
}



void SimpleSamplerComponentAudioProcessorEditor::addButton( Button* aButton, std::string aName)
{
    addChildComponent(aButton);

}
void SimpleSamplerComponentAudioProcessorEditor::configureButton( Button* aButton, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aButton->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aButton->setLookAndFeel(laf);
        }
        if( uec->param != "") {
            aButton->setButtonText(uec->param);
        }
        aButton->setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addAndConfigureButton( Button* aButton, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
		addChildComponent(aButton);
		if( uec->hasBounds()) {
            aButton->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aButton->setLookAndFeel(laf);
        }
        if( uec->param != "") {
            aButton->setButtonText(uec->param);
        }
        
        aButton->setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addComponent( Component* aComponent, std::string aName)
{
    addChildComponent(aComponent);
    
}

void SimpleSamplerComponentAudioProcessorEditor::configureComponent( Component* aComponent, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
        if( uec->hasBounds()) {
            aComponent->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        if( uec->hasSize()) {
            aComponent->setSize(uec->sizeX, uec->sizeY);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aComponent->setLookAndFeel(laf);
        }
        
        aComponent->setVisible(uec->visible);
    }
}

void SimpleSamplerComponentAudioProcessorEditor::addAndConfigureComponent( Component* aComponent, std::string aName)
{
    UIElementConfig* uec = uiConfig->getElementConfig(aName);
    if( uec != NULL ) {
		addChildComponent(aComponent);
        if( uec->hasBounds()) {
            aComponent->setBounds( uec->x, uec->y, uec->w, uec->h);
        }
        if( uec->hasSize()) {
            aComponent->setSize(uec->sizeX, uec->sizeY);
        }
        LookAndFeel* laf = uiConfig->getLookAndFeel(uec->lookAndFeel);
        if( laf != nullptr ) {
            aComponent->setLookAndFeel(laf);
        }
        
        aComponent->setVisible(uec->visible);
    }
}




void SimpleSamplerComponentAudioProcessorEditor::resized()
{
}



void SimpleSamplerComponentAudioProcessorEditor::setPatchDefinition(PatchDefinition* aPatchDefinition)
{
	patchDefinition = aPatchDefinition;
	if( patchDefinition != NULL) {
        updateUIControls();
	}
}




void SimpleSamplerComponentAudioProcessorEditor::buttonClicked (Button* buttonThatWasClicked)
{
	if( buttonThatWasClicked == reloadPatchesButton) {
        
		((SimpleSamplerComponentAudioProcessor*)this->getAudioProcessor())->reloadPatches();
    } else if( buttonThatWasClicked == reloadUIButton) {
            
        ((SimpleSamplerComponentAudioProcessor*)this->getAudioProcessor())->reloadUI();
        updateUIControls();
    } else if( buttonThatWasClicked == aboutButton) {
        AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::InfoIcon, "About", uiConfig->licenceText, "OK", NULL,NULL);
    }
    

}


void SimpleSamplerComponentAudioProcessorEditor::updateUIControls()
{
    if( midiKeyboard) {
        midiKeyboard->setPlayRange( patchDefinition->playRangeFromMidiNote, patchDefinition->playRangeToMidiNote);
        midiKeyboard->setKeyswitchRange( patchDefinition->keySwitchRangeFromMidiNote, patchDefinition->keySwitchRangeToMidiNote);
        midiKeyboard->setPlayRangeShift( 0 );
        midiKeyboard->setKeyswitchRangeShift(0);
        midiKeyboard->centerVisibleRange();
    }
    if( soundSetCombo ) {
        StringArray& soundSetNames = patchDefinition->getSoundSetNames();
        soundSetCombo->setSelectionTexts( soundSetNames );
    }
}

