/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Sampler.hpp"
#include "SampleBin.hpp"
#include "PatchDefinition.hpp"
#include "UIConfiguration.hpp"
#include "PluginParameters.hpp"
#include "DFDThreadPool.hpp"




/*--------------------------------------------------------------------------------------------------------------
 * SimpleSamplerComponentAudioProcessor
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class SimpleSamplerComponentAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    SimpleSamplerComponentAudioProcessor();
    ~SimpleSamplerComponentAudioProcessor();
    
    //==============================================================================
    // LOAD / RELOAD CONFIGURATION
    //==============================================================================
    void cleanupPatches();
    bool loadSampleBin();
    bool loadPatches();
    bool loadUIConfiguration();
    void reloadPatches();
    void reloadUI();
    

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

#ifndef JucePlugin_PreferredChannelConfigurations
	bool isBusesLayoutSupported(const BusesLayout& layouts) const override;
#endif

    //==============================================================================
    void processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages) override
    {
        jassert (! isUsingDoublePrecision());
        process (buffer, midiMessages);
    }
    
    void processBlock (AudioBuffer<double>& buffer, MidiBuffer& midiMessages) override
    {
        jassert (isUsingDoublePrecision());
        process (buffer, midiMessages);
    }
    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    
    void setPatch( PatchDefinition& aPatchDefinition);

    PatchDefinition* getCurrentPatchDefinition();
    PluginParameters* getPluginParameters() { return &pluginParameters; };
    
    // this is kept up to date with the midi messages that arrive, and the UI component
    // registers with it so it can represent the incoming messages
    MidiKeyboardState keyboardState;
    

private:
    ScopedPointer<FileLogger> flogger;

    juce::String aPathToAU;

    float sourceSampleRate;
    SimpleSamplerComponentAudioProcessorEditor* editor;
    Sampler sampler;
    SampleBin sampleBin;
    //std::vector<PatchDefinition*> patchDefinitions;
    PatchBank patchBank;
    UIConfig uiConfig;
    PatchDefinition* currentPatchDefinition;
    int currentPatchNdx;
    PluginParameters pluginParameters;
    
    
    /*PatchDefinition* findPatchDefinition( std::string aName);
    int findPatchIndex( std::string aName);*/
    void createParameters();

    //StringArray getProgramNames();

    template <typename FloatType>
    void process (AudioBuffer<FloatType>& buffer, MidiBuffer& midiMessages);
    
    // The ThreadPool that will manage the background reading
    ScopedPointer<ThreadPool> dfdThreadPool;


    //==============================================================================
    
    StringArray getControllersStringArray();

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleSamplerComponentAudioProcessor)
    
    
    
};


#endif  // PLUGINPROCESSOR_H_INCLUDED
